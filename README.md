# TOTP Demo

Demos the setup and verification of TOTP codes.

## Running the demo
To run the demo, you will need the dotnet 8 runtime and SDK: https://dotnet.microsoft.com/en-us/download/dotnet/8.0. Then use the following command in a terminal from the root of this repository:

``` shell
dotnet run
```

## Setup
To set up TOTP code verification, you will need a byte array secret that is stored on the server side and will be accessible during verification. The TOTP setup URL will use the Base32-encoded version of this byte array. Generating the URL is as follows:

``` csharp
// Get byte[] key
string base32EncodedKey = Base32Encoding.ToString(key).TrimEnd('=');
Console.WriteLine($"URL: otpauth://totp/demo?secret={base32EncodedKey}");
```

## Verification
To verify TOTP codes, use the byte array secret to verify the code within the verification window of your choosing. In this example, I will use one step to either side (to accomodate for skew and network delay). Verifying totp codes is as follows:

``` csharp
// Get byte[] key
string code = Console.ReadLine()!;
var totp = new Totp(key);
bool didVerify = totp.VerifyTotp(code, out _, new VerificationWindow(previous: 1, future: 1));
Console.WriteLine(didVerify ? "Yes" : "No");
```