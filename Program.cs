﻿using System.Text;
using OtpNet;

// Generating URL
byte[] key = Encoding.UTF8.GetBytes("abc123");
string base32EncodedKey = Base32Encoding.ToString(key).TrimEnd('=');
Console.WriteLine($"URL: otpauth://totp/demo?secret={base32EncodedKey}");

// Verifying URL
Console.WriteLine("Enter code");
string code = Console.ReadLine()!;
var totp = new Totp(key);
bool didVerify = totp.VerifyTotp(code, out _, new VerificationWindow(previous: 1, future: 1));
Console.WriteLine(didVerify ? "Yes" : "No");